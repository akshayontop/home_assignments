from flask import Flask, Session, session, render_template, request
import os

app = Flask(__name__)
app.secret_key = os.urandom(24)


@app.route('/', methods=['GET', 'POST'])
def index():
    return render_template('index.html')


@app.route('/getsession', methods=['POST'])
def getsession():
    name = request.form['name']
    session['user'] = name
    return 'Not Logged In'


if __name__ == '__main__':
    app.run(debug=True)
