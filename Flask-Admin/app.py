from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from sqlalchemy import Integer, Column, String, ForeignKey

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///testMyDb1.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = 'mykey'
db = SQLAlchemy(app)

admin = Admin(app)


class User(db.Model):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    password = Column(String)
    units = Column(String)
    workouts = db.relationship('Workout', backref='user', lazy='dynamic')

    def __repr__(self):
        return '{}'.format(self.name)


class Workout(db.Model):
    __tablename__ = 'workout'
    id = Column(Integer, primary_key=True)
    date = Column(String)
    user_id = db.Column(Integer, db.ForeignKey('user.id'))
    notes = Column(String)
    bodyweight = Column(db.Numeric)
    exercises = db.relationship('Exercise', backref='workout', lazy='dynamic')

    def __repr__(self):
        return '{}'.format(self.id)


class Exercises(db.Model):
    __tablename__ = 'exercises'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    exercise = db.relationship('Exercise', backref='exercise', lazy='dynamic')

    def __repr__(self):
        return '{}'.format(self.name)


class Exercise(db.Model):
    __tablename__ = 'exercise'
    id = Column(Integer, primary_key=True)
    workout_id = db.Column(db.Integer, ForeignKey('workout.id'), primary_key=True)
    order = Column(Integer, primary_key=True)
    exercise_id = Column(Integer, ForeignKey('exercises.id'))
    sets = db.relationship('Set', backref='exercise', lazy='dynamic')

    def __repr__(self):
        return '{}'.format(self.id)


class ExerciseView(ModelView):
    form_columns = ['id', 'workout', 'order', 'exercise']


class Set(db.Model):
    __tablename__ = 'set'
    id = Column(Integer, primary_key=True)
    order = Column(Integer, primary_key=True)
    weight = Column(db.Numeric)
    reps = Column(Integer)
    exercise_id = Column(Integer, ForeignKey('exercise.id'))

    def __repr__(self):
        return '{}'.format(self.id)


class SetView(ModelView):
    form_columns = ['id', 'order', 'weight', 'reps', 'exercise']


class Temp(db.Model):
    __tablename__ = 'temp'
    id = Column(Integer, primary_key=True)


class TestModelView(ModelView):
    def testThis(self):
        return False


admin.add_view(ModelView(User, db.session))
admin.add_view(ModelView(Workout, db.session))
admin.add_view(ExerciseView(Exercise, db.session))
admin.add_view(ModelView(Exercises, db.session))
admin.add_view(SetView(Set, db.session))
admin.add_view(TestModelView(Temp, db.session))
db.create_all()

if __name__ == '__main__':
    app.run(debug=True)
