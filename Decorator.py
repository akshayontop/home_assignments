'''
def messageWithWelcome(str):

    def addWelcome():
        return "Welcome to "

    return addWelcome()+str

def myFunction(name):
    return name

print(messageWithWelcome(myFunction("The RedPanda Family ")))

'''

def check(func):
    def inside(a,b):
        if b==0:
            print("Can't divide by 0")
            return
        func(a,b)
    return inside

@check
def div(a,b):
    return a/b

print(div(5,0))