class Base(object):
    def __init__(self, name):
        self.name = name
        print(self.name)


class Derived(Base):
    def __init__(self, num1, num2):
        super(Derived, self).__init__(num1)
        self.num2 = num2
        print(self.num2)


obj=Derived(5,10)