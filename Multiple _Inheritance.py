'''
class Parent1(object):
    def __init__(self):
        print("Paerent1")
        self.str = "1"


class Parent2(object):
    def __init__(self):
        print("Parent2")
        self.str = "2"


class Derived(Parent1, Parent2):
    def __init__(self):
        Parent1.__init__(self)
        Parent2.__init__(self)
        print("Derived")


ob = Derived()


'''


class Base(object):
    x = 10

    def __init__(self):
        print("in base", self.x)


class Derived(Base):
    def __init__(self):
        Base.__init__(self)
        Base.x = 14
        print(Base.x)



d=Derived()